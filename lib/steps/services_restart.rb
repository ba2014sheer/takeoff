# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class ServicesRestart < BaseRoles
    CONCURRENT_SERVICES = %w[unicorn nginx gitlab-workhorse gitlab-pages]

    def run
      if (roles.services_for_role(role) & %w[unicorn gitlab-pages gitaly]).any?
        custom_services.each { |service| restart_services(service) }
      else
        restart_services
      end
    end

    private

    def role
      options[:role]
    end

    def custom_services
      roles.services_for_role(role, skip: skip_services + %w[unicorn])
    end

    def restart_services(service = nil)
      cmd = 'sudo gitlab-ctl restart'
      cmd += " #{service}" if service

      if concurrent_service?(service)
        run_command_concurrently role,
                                 cmd,
                                 title: "Restarting #{service_text(service)} concurrently on #{role}",
                                 spinners: spinners,
                                 command_wrapper: options[:command_wrapper]

      else
        run_command_on_roles role,
                             cmd,
                             title: "Restarting #{service_text(service)} on #{role}",
                             spinners: spinners
      end
    end

    def service_text(service)
      service ? service : 'services'
    end

    def skip_services
      options[:skip_services] || []
    end

    def concurrent_service?(service)
      service && CONCURRENT_SERVICES.include?(service)
    end
  end
end
