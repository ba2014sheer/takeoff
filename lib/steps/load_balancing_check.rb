# frozen_string_literal: true

require './lib/steps/base_roles'
require './config/takeoff'
require 'fileutils'
require 'json'

module Steps
  class LoadBalancingCheck < BaseRoles
    def run
      run_with_progress "bundle exec knife download #{blessed_node_role_path}",
                        title: 'Downloading blessed node JSON'

      if Takeoff.config[:dry_run]
        puts 'Ignoring load balancing check'
      else
        balancing_enabled = db_load_balancing && !db_load_balancing.empty?

        FileUtils.rm(blessed_node_role_path)

        abort ANSI::RED % 'db_load_balancing is not empty on the blessed node!' if balancing_enabled
      end
    end

    private

    def blessed_node_role_path
      @blessed_node_role_path ||= "roles/#{blessed_node}.json"
    end

    def blessed_node
      @blessed_node ||= roles.blessed_node
    end

    def role_json
      JSON.parse(File.read(blessed_node_role_path))
    end

    def db_load_balancing
      @db_load_balancing ||= role_json.dig(%w[override_attributes omnibus-gitlab gitlab_rb gitlab-rails db_load_balancing])
    end
  end
end
