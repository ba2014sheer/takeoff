module TimeUtil
  def time_elapsed(duration, include_ago: true)
    time = case duration.to_i
           when 0..59 then
             "#{duration.to_i}s"
           when 59..119 then
             '1 minute'
           when 120..3599 then
             "#{duration.to_i / 60} minutes"
           when 3600..86_399 then
             "#{(duration.to_f / (60 * 60)).round(1)} hours"
           else
             "#{(duration.to_f / (60 * 60 * 24)).round(1)} days"
           end

    include_ago ? "#{time} ago" : time
  end
end
