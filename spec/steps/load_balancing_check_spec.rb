# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/load_balancing_check'

describe Steps::LoadBalancingCheck do
  subject { described_class.new(Roles.new('gstg')) }

  context 'with dry_run' do
    before { enable_dry_run }

    it 'ignores check on dry run' do
      expect { subject.run }.to output(/Ignoring load balancing check/).to_stdout_from_any_process
    end

    it 'downloads the JSON' do
      expect { subject.run }.to output(%r{bundle exec knife download roles/gstg-base-deploy-node.json}).to_stdout_from_any_process
    end
  end

  context 'without dry_run' do
    before do
      allow(Takeoff).to receive(:config) { { dry_run: false } }
    end

    it 'removes the JSON' do
      json_role_path = 'spec/fixtures/roles/gstg-base-deploy-node.json'

      allow(subject).to receive(:run_with_progress)
      allow(subject).to receive(:blessed_node_role_path) { json_role_path }

      expect(FileUtils).to receive(:rm).with(json_role_path)

      subject.run
    end
  end
end
