#!/usr/bin/env ruby
# frozen_string_literal: true

#
# Example usage: tail

require 'optparse'

class Parser
  def self.parse(options)
    args = {}

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: version [options]'

      opts.on('-e', '--env=gprd', 'Environment i.e gstg. Defaults to gprd.') do |env|
        args[:env] = env
      end

      opts.on('-v', '--version=VERSION', 'GitLab version i.e 10.1.0-rc1.ee.0') do |v|
        args[:version] = v
      end

      opts.on('-p', '--pid=PID', 'PID that ends the tail') do |p|
        args[:pid] = p
      end

      opts.on('-n', '--number=123', 'number of lines to tail') do |n|
        args[:lines] = n
      end

      opts.on('-f', 'uses tail --follow') do |f|
        args[:follow] = f
      end

      opts.on('-d', 'tails debug log') do |d|
        args[:debug] = d
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts

        exit
      end
    end

    opt_parser.parse!(options)
    args
  end
end

options = Parser.parse(ARGV)

log_file = if options[:version]
             info = options[:debug] ? '' : '.info'
             prefix = options[:env] || 'warmup'

             "log/#{prefix}-#{options[:version]}#{info}.log"
           else
             logs = Dir['log/*'].reject do |file|
               file.end_with?('.json.log') || (options[:debug] && file.end_with?('.info.log'))
             end

             logs.max_by { |f| File.mtime(f) }
           end

lines = options[:lines] || 50

tail_string = +'tail'
tail_string << ' -f --retry' if options[:follow]
tail_string << " --pid=#{options[:pid]}" if options[:pid]
tail_string << " -n #{lines} #{log_file}"

system(tail_string)
